La parte UI del chatbot � stata creata, per velocit� di creazione del POC, con il servizio chatFuel.
A questa url https://dashboard.chatfuel.com/#/invite/lUczU8l9MD8fuUG6pi569f6xPiBAinr0 si pu� diventare admin e visionare il lavoro svolto.

Lato backend sono state create le API per la comunicazione. Tali API si possono vedere richiamate nel flow del chatbot e nella documentazione apidoc.