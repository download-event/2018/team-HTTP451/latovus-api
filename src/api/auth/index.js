import { Router } from 'express';

import { password, token } from '../../services/passport';
import actions from './controller';

const router = new Router();

/**
 * @api {post} /auth Authenticate
 * @apiName Authenticate
 * @apiGroup Auth
 * @apiPermission basic
 * @apiHeader {String} Authorization Basic authorization with email and password.
 * @apiSuccess (Success 201) {String} token User `access_token` to be passed to other requests.
 * @apiError 401 Master access only or invalid credentials.
 **/
router.post('/', password(), actions.login);

/**
 * @api {get} /checkJWT Check JWT Validity
 * @apiName Check JWT Validity
 * @apiGroup Auth
 * @apiPermission token
 * @apiParam {String} access_token access_token.
 * @apiSuccess (Success 200) {String} OK
 * @apiError 401 Master access only or invalid credentials.
 **/
router.get('/checkJWT', token({ required: true }), actions.checkJWT);

/**
 * @api {get} /renewJWT Renew JWT
 * @apiName Renew JWT
 * @apiGroup Auth
 * @apiPermission token
 * @apiParam {String} access_token access_token.
 * @apiSuccess (Success 201) {String} token User `access_token` to be passed to other requests.
 * @apiSuccess (Success 201) {Object} user Current user's data.
 * @apiError 401 Master access only or invalid credentials.
 **/
router.get('/renewJWT', token({ required: true }), actions.login);

export default router;
