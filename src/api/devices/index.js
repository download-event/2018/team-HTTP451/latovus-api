import { setupDevicesApi } from '@becodebg/odin-api-pushnotifications';
import mongoose from 'mongoose';

import { master } from '../../services/passport';

/**
 * @api {js} device Schema
 * @apiGroup Devices
 * @apiName Schema
 * @apiExample {js} Entity schema:
{
  os: {
    type: String,
    required: true
  },
  token: {
    type: String
  },
  locale: {
    type: String
  },
  userId: {
    type: Schema.ObjectId,
    ref: 'User'
  }
}; **/

const { router, entity: model } = setupDevicesApi({
  authMiddleware: master(),
  mongooseInstance: mongoose
});

export { model };

/**
 * @api {post} /devices Create device
 * @apiGroup Devices
 * @apiName CreateDevice
 * @apiPermission master
 * @apiSuccess (Success 201) {Device} device Device's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 Master access only.
 **/

/**
 * @api {put} /devices/:id Update device
 * @apiGroup Devices
 * @apiName UpdateDevice
 * @apiPermission master
 * @apiSuccess {Device} device Device's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 Master access only.
 * @apiError 404 Device not found.
 **/

export default router;
