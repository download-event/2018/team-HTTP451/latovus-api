import { ControllerGenerator } from '@becodebg/odin-generators';
import ssimilarity from 'string-similarity';
import Promise from 'bluebird';
import { success, notFound } from '@becodebg/odin-services-response';

import Entity from './model';

const MINIMUM_SIMILARITY = 0.6;

const actions = ControllerGenerator(Entity);

actions.duplicatesforChatbot = (req, res, next) => Entity.find({
  coordinates: {
    $near: {
      $maxDistance: req.params.distance || 300,
      $geometry: {
        type: 'Point',
        coordinates: [
          req.body.latitude !== null ? req.body.latitude : 0,
          req.body.longitude !== null ? req.body.longitude : 0
        ]
      }
    }
  },
  category: req.body.category
})
  .exec()
  .then(elements => {
    return elements.reduce((accumulator, currentValue) => {
      const similarity = ssimilarity.compareTwoStrings(
        req.body.description,
        currentValue.description || ''
      );
      if (similarity > (req.params.affinity || MINIMUM_SIMILARITY)) {
        return {
          'messages': [
            { 'text': 'Presente segnalazione simile!' }
          ]
        };
      }
      return {};
    }, []);
  })
  .then(success(res))
  .catch(next);

actions.duplicates = (req, res, next) => Entity.find({
  coordinates: {
    $near: {
      $maxDistance: req.params.distance || 300,
      $geometry: {
        type: 'Point',
        coordinates: [
          req.body.coordinates[0] !== null ? req.body.coordinates[0] : 0,
          req.body.coordinates[1] !== null ? req.body.coordinates[1] : 0
        ]
      }
    }
  },
  category: req.body.category
})
  .exec()
  .then(elements => {
    return elements.reduce((accumulator, currentValue) => {
      const similarity = ssimilarity.compareTwoStrings(
        req.body.description,
        currentValue.description || ''
      );
      if (similarity > (req.params.affinity || MINIMUM_SIMILARITY)) {
        accumulator.push({
          _id: currentValue._id,
          description: currentValue.description,
          similarity: similarity
        });
      }
      return accumulator;
    }, []);
  })
  .then(similarEntities => {
    return similarEntities.sort((a, b) => a < b);
  })
  .then(success(res))
  .catch(next);

actions.duplicatesById = (req, res, next) => Entity.findById(req.params.id)
  .exec()
  .then(issue => {
    return Entity.find({
      coordinates: {
        $near: {
          $maxDistance: 300,
          $geometry: {
            type: 'Point',
            coordinates: [
              issue.coordinates[0] !== null ? issue.coordinates[0] : 0,
              issue.coordinates[1] !== null ? issue.coordinates[1] : 0
            ]
          }
        }
      },
      category: issue.category
    })
      .exec()
      .then(elements => {
        return elements.reduce((accumulator, currentValue) => {
          const similarity = ssimilarity.compareTwoStrings(
            issue.description,
            currentValue.description || ''
          );
          if (similarity > req.params.affinity || MINIMUM_SIMILARITY) {
            accumulator.push({
              _id: currentValue._id,
              description: currentValue.description,
              similarity: similarity,
              category: currentValue.category
            });
          }
          return accumulator;
        }, []);
      })
      .then(similarEntities => {
        return similarEntities.sort((a, b) => a < b);
      })
      .then(success(res))
      .catch(next);
  });

actions.near = (req, res, next) => Entity.find({
  coordinates: {
    $near: {
      $maxDistance: req.params.distance || 300,
      $geometry: {
        type: 'Point',
        coordinates: [
          req.body.latitude !== null ? req.body.latitude : 0,
          req.body.longitude !== null ? req.body.longitude : 0
        ]
      }
    }
  },
  category: req.body.category
})
  .exec()
  .then(notFound(res))
  .then(success(res))
  .catch(next);

actions.stats = (
  { querymen: { query, select, cursor, view }, query: querystring },
  res,
  next
) => Entity.find(query, select, cursor)
  .smartPopulate([
    {
      path: 'created_by',
      select: '-password'
    }
  ])
  .exec()
  .then(entities => Promise.map(entities, entity => entity.view(select, querystring, view))
  )
  .then(entities => entities.reduce(
    (accumulator, entity) => {
      accumulator.totalTickets++;
      if (accumulator[entity.category]) {
        accumulator[entity.category].total++;
      } else {
        accumulator[entity.category] = { total: 1 };
      }

      if (accumulator[entity.category][entity.status]) {
        accumulator[entity.category][entity.status]++;
      } else {
        accumulator[entity.category][entity.status] = 1;
      }

      return accumulator;
    },
    { totalIssues: 0 }
  )
  )
  .then(success(res))
  .catch(next);

export { actions };
