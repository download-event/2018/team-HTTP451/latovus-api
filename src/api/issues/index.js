import { Router } from 'express';
import { middleware as body } from 'bodymen';
import { middleware as query } from 'querymen';

import { token } from '../../services/passport';

import { actions } from './controller';
import { bodymenSchema } from './model';

const router = new Router();

/**
 * @api {get} /issues List issues
 * @apiGroup Issue
 * @apiName ListIssues
 * @apiPermission user
 * @apiUse listParams
 * @apiSuccess {Issue[]} issues List of Issues.
 **/
router.get(
  '/',
  token({ required: true }),
  query(bodymenSchema.query),
  actions.index
);

/**
 * @api {get} /issues/stats List issues stats
 * @apiGroup Issue
 * @apiName ListIssuesStats
 * @apiPermission user
 * @apiUse listParams
 * @apiSuccess {Issue[]} issues List of Issues.
 **/
router.get(
  '/stats',
  token({ required: true }),
  query(bodymenSchema.query),
  actions.stats
);

/**
 * @api {get} /issues/:id Retrieve issue
 * @apiGroup Issue
 * @apiName RetrieveIssue
 * @apiPermission user
 * @apiSuccess {Issue} issue Issue's data.
 * @apiError 404 Issue not found.
 **/
router.get('/:id', token({ required: true }), actions.show);

/**
 * @api {post} /issues/duplicates Retrieve issue
 * @apiGroup Issue
 * @apiName GetPotentialDuplicatedIssues
 * @apiPermission user
 * @apiSuccess {Issue} issue Issue's data.
 * @apiError 404 Issue not found.
 **/
router.post('/duplicates', body(bodymenSchema.creation), actions.duplicates);

/**
 * @api {post} /issues/duplicatesForChatbot Retrieve issue
 * @apiGroup Issue
 * @apiName GetPotentialDuplicatedIssues
 * @apiPermission user
 * @apiSuccess {Issue} issue Issue's data.
 * @apiError 404 Issue not found.
 **/
router.post('/duplicatesForChatbot', body(bodymenSchema.creation), actions.duplicatesforChatbot);

/**
 * @api {get} /issues/duplicates/:id Retrieve issues id
 * @apiGroup Issue
 * @apiName GetPotentialDuplicatedIssues
 * @apiPermission user
 * @apiSuccess {Issue} issue Issue's data.
 * @apiError 404 Issue not found.
 **/
router.get('/duplicates/:id', token({ required: true }), actions.duplicatesById);

/**
 * @api {post} /near Retrieve near issues by lat, long, description
 * @apiGroup Issue
 * @apiName Near
 * @apiPermission user
 * @apiSuccess {Issue} issue Issue's data.
 * @apiError 404 Issue not found.
 **/
router.post('/near', body(bodymenSchema.creation), actions.near);

/**
 * @api {post} /issues Create issue
 * @apiGroup Issue
 * @apiName CreateIssue
 * @apiPermission user
 * @apiSuccess (Success 201) {Issue} issue new Issue's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 **/
router.post('/', body(bodymenSchema.creation), actions.create);

/**
 * @api {put} /issues/:id Update issue
 * @apiGroup Issue
 * @apiName UpdateIssue
 * @apiPermission user
 * @apiSuccess {Issue} issue Issue's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Issue not found.
 **/
router.put(
  '/:id',
  token({ required: true }),
  body(bodymenSchema.update),
  actions.update
);

/**
 * @api {delete} /issues/:id Delete Issue
 * @apiGroup Issue
 * @apiName DeleteIssue
 * @apiPermission user
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Issue not found.
 **/
router.delete('/:id', token({ required: true }), actions.destroy);

export Entity from './model';

export default router;
