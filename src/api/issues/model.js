import { getValidationSchema, ModelGenerator } from '@becodebg/odin-generators';
import mongoose, { Schema } from 'mongoose';
import { Entity as User } from '../users';

const mongooseSchema =
  /**
   * @api {js} issues Schema
   * @apiGroup Issue
   * @apiName Schema
   * @apiExample {js} Entity schema: */
  {
    category: {
      type: String,
      enum: [
        'Buca',
        'Lampione',
        'BlackOut',
        'Scarico abusivo di rifiuti',
        'Altro'
      ]
    },
    coordinates: {
      type: [Number], // [<longitude>, <latitude>]
      index: '2dsphere',
      default: [0, 0]
    },
    coordinatesEstimation: {
      type: [Number], // [<longitude>, <latitude>]
      index: '2dsphere',
      default: [0, 0]
    },
    typedAddress: { type: String },
    description: { type: String },
    userInsertedKeywords: { type: [String] },
    computatedKeywords: { type: [String] },
    status: {
      type: String,
      enum: ['Aperta', 'Presa in carico', 'Risolta'],
      default: 'Aperta'
    },
    authorId: {
      type: Schema.ObjectId,
      odinVirtualPopulation: {
        odinAutoPopulation: true,
        fieldName: 'author',
        options: {
          ref: 'User'
        }
      }
    },
    source: {
      type: String,
      enum: ['APP', 'FACEBOOK_BOT', 'OTHER'],
      default: 'APP'
    },
    isAnonymous: {
      type: Boolean
    },
    facebookIdIfAnonymous: {
      type: String
    },
    chatfuel: {
      type: Object
    },
    plusOnes: {
      type: Number,
      default: 0
    },
    pictureId: {
      type: Schema.ObjectId,
      odinVirtualPopulation: {
        odinAutoPopulation: true,
        fieldName: 'picture',
        options: {
          ref: 'UploadedFile'
        }
      }
    },
    facebookPictureUrl: {
      type: String
    },
    latitude: {
      type: Number,
      odinVirtual: true
    },
    longitude: {
      type: Number,
      odinVirtual: true
    },
    'chatfuel user id': {
      type: String,
      odinVirtual: true
    },
    chatfuelIsAnonymous: {
      type: String,
      odinVirtual: true
    },
    image: {
      type: String,
      odinVirtual: true
    },
    'first name': {
      type: String
    },
    'last name': {
      type: String
    },
    note: {
      type: String
    }
  };
/* **/

const model = ModelGenerator(mongoose)({
  schema: mongooseSchema,
  collectionName: 'issues',
  modelName: 'Issue',
  populationOptions: [],
  extensionFunction: schema => {
    schema.virtual('latitude').set(function(value) {
      this.coordinates[1] = this.coordinates[1] || value || 0;
    });
    schema.virtual('longitude').set(function(value) {
      this.coordinates[0] = this.coordinates[0] || value || 0;
    });
    schema.virtual('chatfuel user id').set(function(value) {
      this.facebookIdIfAnonymous = value;
    });
    schema.virtual('chatfuelIsAnonymous').set(function(value) {
      this.isAnonymous = value === 'Si';
    });
    schema.virtual('pictureUrl').get(function() {
      return this.picture && this.picture.full
        ? this.picture.full
        : this.facebookPictureUrl;
    });
    schema.virtual('image').set(function(value) {
      this.facebookPictureUrl = value;
    });
    schema.pre('save', function(next) {
      if (
        !this.isAnonymous &&
        this.facebookIdIfAnonymous &&
        this.isModified('facebookIdIfAnonymous')
      ) {
        User.findOne({ facebookId: this.facebookIdIfAnonymous })
          .then(user => {
            if (!user) {
              return User.create({
                name: this['first name'] + ' ' + this['last name'],
                email:
                  this['first name'] +
                  '.' +
                  this['last name'] +
                  '@' +
                  this['last name'] +
                  '.com',
                facebookId: this.facebookIdIfAnonymous,
                password: 123456 // best practice
              }).then(created => {
                this.authorId = created._id;
                if (this.isAnonymous) {
                  this['first name'] = null;
                  this['last name'] = null;
                }
                this.source = 'FACEBOOK_BOT';
                this.save();
                next();
              });
            }
            if (!this.isAnonymous) {
              this.authorId = user._id;
            }
          })
          .then(() => next());
      }
      if (this.isAnonymous) {
        this['first name'] = null;
        this['last name'] = null;
      }
      next();
    });
  }
});

export const schema = model.schema;
export const bodymenSchema = getValidationSchema(mongooseSchema);

export default model;
