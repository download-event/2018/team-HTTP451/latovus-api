import { Router } from 'express';
import { middleware as body } from 'bodymen';
import { middleware as query } from 'querymen';

import { token } from '../../services/passport';
import { actions } from './controller';
import { schema, bodymenSchema } from './model';

const router = new Router();

/**
 * @api {get} /notifications Retrieve notifications
 * @apiGroup Notifications
 * @apiName RetrieveNotifications
 * @apiPermission user
 * @apiParam {String} access_token User access_token.
 * @apiUse listParams
 * @apiSuccess {Notification[]} notifications List of notifications.
 * @apiError 401 Admins access only.
 **/
router.get('/', token({ required: true }), query(bodymenSchema.query), actions.showUserNotifications);

export Entity, { schema } from './model';

export default router;
