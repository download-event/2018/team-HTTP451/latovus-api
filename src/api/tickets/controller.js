import { ControllerGenerator } from '@becodebg/odin-generators';
import { success } from '@becodebg/odin-services-response';
import Promise from 'bluebird';
import Entity from './model';

const actions = ControllerGenerator(Entity);

actions.stats = (
  { querymen: { query, select, cursor, view }, query: querystring },
  res,
  next
) => Entity.find(query, select, cursor)
    .smartPopulate([
      {
        path: 'created_by',
        select: '-password'
      }
    ])
    .exec()
    .then(entities => Promise.map(entities, entity => entity.view(select, querystring, view))
    )
    .then(entities => entities.reduce(
        (accumulator, entity) => {
          accumulator.totalTickets++;
          if (accumulator[entity.department]) {
            accumulator[entity.department].total++;
          } else {
            accumulator[entity.department] = { total: 1 };
          }

          if (accumulator[entity.department][entity.status]) {
            accumulator[entity.department][entity.status]++;
          } else {
            accumulator[entity.department][entity.status] = 1;
          }

          return accumulator;
        },
        { totalTickets: 0 }
      )
    )
    .then(success(res))
    .catch(next);

export { actions };
