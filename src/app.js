import fs from 'fs';
import http from 'http';
import https from 'https';
import logger from 'winston';

import api from './api';
import { disableScheduler, env, ip, mongo, port } from './config';
import express from './services/express';
import mongoose from './services/mongoose';
import * as scheduler from './services/scheduler';

const app = express(api);

let server;
if (env === 'development') {
  const sslOptions = {
    key: fs.readFileSync('certs/key.pem'),
    cert: fs.readFileSync('certs/cert.pem'),
    passphrase: 'password'
  };
  server = https.createServer(sslOptions, app);
} else {
  server = http.createServer(app);
}

mongoose.connect(mongo.uri);

setImmediate(() => {
  server.listen(port, ip, () => {
    logger.info(
      `\x1B[0;34mExpress:\x1B[0m Server listening on http${
        env === 'development' ? 's' : ''
      }://${ip}:${port}, in ${env} mode`
    );
  });

  if (!disableScheduler) {
    scheduler.start();
  }
});

export default app;
