import Promise from 'bluebird';
import _ from 'lodash';
import schedule from 'node-schedule';
import wlogger from 'winston';

import { default as Notification } from '../../api/notifications/model';
import { Entity as User } from '../../api/users';
import { appName } from '../../config';
import { PushNotificationService } from '../push-notifications';

const logger = (...message) => wlogger.info('[Scheduler]', ...message);

export const start = function() {
  logger('Starting...');
  schedule.scheduleJob('*/1 * * * *', sendPushNotifications);
  sendPushNotifications();
  logger('Starting...', 'DONE');
};

function sendPushNotifications() {
  logger('[PUSH]', 'Executing unsent Notifications check...');
  Notification.getUnsentNotifications().then(elements => {
    logger('[PUSH]', `${elements.length} unsent Notifications found.`);

    if (elements.length) {
      logger('[PUSH]', `Will send ${elements.length} push notifications.`);

      let userWithNoDevices = [];

      const notificationsByUser = _.groupBy(elements, 'targetUser');
      Promise.map(_.keys(notificationsByUser), userId => {
        const notifications = notificationsByUser[userId];

        return new Promise((resolve, reject) => {
          if (notifications.length > 1) {
            // Send generic push notification
            resolve({
              title: appName,
              body: `You have ${notifications.length} new notifications`
            });
          } else if (notifications.length === 1) {
            // Get text message from Notification
            resolve(notifications[0].getMessage());
          } else {
            reject(`No notifications for user ${userId}.`);
          }
        })
          .then(notificationMessage => Promise.all([User.findById(userId), notificationMessage]))
          .then(([user, message]) => PushNotificationService.sendNotificationToUser(user, {
              ...message,
              badge: notifications.length // Set iOS Badge count
            })
          )
          .then(() => {
            _.each(notifications, n => n.markSent());
          })
          .catch(() => {
            // logger('[ERROR]', err);
            userWithNoDevices.push(userId);
          });
      }).then(() => {
        logger('[PUSH]', 'Users with no devices:', userWithNoDevices.length);
        logger('[PUSH]', 'Executing unsent Notifications check... DONE');
      });
    } else {
      logger('[PUSH]', 'No notifications to be sent.');
    }
  });
}
