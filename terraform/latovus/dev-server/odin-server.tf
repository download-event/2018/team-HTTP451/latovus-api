# Configure S3 module
module "s3" {
  source = "../../../modules/s3storage"

  # AWS access
  aws_access_key_id     = "${var.aws_access_key_id}"
  aws_access_secret_key = "${var.aws_access_secret_key}"

  # S3 setup
  s3_user_name                = "heroku-${var.app_name}-${var.app_environment}-user"
  s3_bucket_name              = "${var.app_name}-${var.app_environment}"
  s3_resource_tag_name        = "${var.app_name}"
  s3_resource_tag_environment = "${var.app_environment}"
}

# Configure Heroku module
module "heroku" {
  source = "../../../modules/heroku"

  # Heroku access
  email   = "${var.heroku_email}"
  api_key = "${var.heroku_api_key}"

  # Heroku setup
  app_name = "${var.app_name}${var.app_environment == "prod" ? "" : "-${var.app_environment}"}"

  s3_bucket_name = "${module.s3.bucket_name}"
  s3_access_key  = "${module.s3.bucket_access_key}"
  s3_secret_key  = "${module.s3.bucket_secret_key}"

  app_sendgrid_key = "${var.app_sendgrid_key}"
}
