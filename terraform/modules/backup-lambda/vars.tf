#
# AWS settings
#

# Authentication for the AWS provider - we use this access
# key in order to be able to create a new AWS user and the S3
# bucket that the application needs.
variable "aws_access_key_id" {}

variable "aws_access_secret_key" {}

variable "aws_region" {
  default     = "eu-west-1"
  description = "AWS Region. Defaults to 'EU (Ireland)'."
}

# This is the name of the application that needs backups
variable "app_name" {
  description = "Name of the application that needs backups."
}

variable "mongodb_url" {
  description = "URL of the MongoDB to backup."
}
