#
# HEROKU settings
#

# Authentication for the Heroku provider - we use this access
# key in order to be able to create a new Heroku application
# and provision the addon

variable "email" {
  description = "Heroku account email"
}

variable "api_key" {
  description = "Heroku account API key"
}

# This is the name of the Heroku application that we will create
# This needs to be unique (no to accounts can have the same name)
variable "app_name" {
  description = "Name of the Heroku application that will be created. This needs to be unique (no two accounts can have the same name)"
}

variable "app_region" {
  default = "eu"
}

# The application needs to provision a database.
# This the plan level for the basic mLab MongoDB database
variable "db_plan" {
  default     = "mongolab:sandbox"
  description = "mLab Database plan - defaults to 'Sandbox'"
}

# The application uses Papertrail to collect logs.
# This the plan level for the basic Papertrail logger
variable "logger_plan" {
  default     = "papertrail:choklad"
  description = "Papertrail logger plan - defaults to 'Choklad'"
}

#
# APP settings
#

# SendGrid API key
variable "app_sendgrid_key" {
  description = "API Key for SendGrid service"
}
