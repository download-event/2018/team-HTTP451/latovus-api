#
# AWS and S3
#

# We need the AWS provider in order to create the S3 bucket
provider "aws" {
  access_key = "${var.aws_access_key_id}"
  secret_key = "${var.aws_access_secret_key}"
  region     = "${var.aws_region}"
}

# Creates the IAM key for write access to the S3 bucket
# We need to create the IAM users, give that user an access
# key, and finally give that user write access to the bucket
# with a policy
resource "aws_iam_user" "iam_user_s3_rw" {
  name = "${var.s3_user_name}"

  # You cannot tag a user, but you can give them a path
  # to help identify the context of the user
  path = "/${var.s3_resource_tag_name}/"
}

# Creates the API key for the user
resource "aws_iam_access_key" "s3_rw" {
  user = "${aws_iam_user.iam_user_s3_rw.name}"
}

# Restricts the user to only the S3 bucket they should have access to
data "aws_iam_policy_document" "s3_policy_rw" {
  statement {
    actions   = ["s3:*"]
    resources = ["arn:aws:s3:::${var.s3_bucket_name}","arn:aws:s3:::${var.s3_bucket_name}/*"]
  }
}

# Restricts the user to only the S3 bucket they should have access to
resource "aws_iam_user_policy" "policy_s3_rw" {
  # We concatenate the user name with the policy to ensure that
  # the policy name is unique, but still recognizable
  name = "${aws_iam_user.iam_user_s3_rw.name}-policy"

  user = "${aws_iam_user.iam_user_s3_rw.name}"

  policy = "${data.aws_iam_policy_document.s3_policy_rw.json}"
}

resource "aws_s3_bucket" "aws_bucket_static" {
  bucket = "${var.s3_bucket_name}"

  tags {
    project   = "${var.s3_resource_tag_name}"
    level     = "${var.s3_resource_tag_environment}"
    target    = "storage"
    Terraform = "true"
  }

  acl = "private"

  cors_rule {
    allowed_origins = ["*"]
    allowed_methods = ["GET"]
    max_age_seconds = 3000
    allowed_headers = ["*"]
  }
}

output "bucket_name" {
  value = "${var.s3_bucket_name}"
}

output "bucket_access_key" {
  value = "${aws_iam_access_key.s3_rw.id}"
}

output "bucket_secret_key" {
  value = "${aws_iam_access_key.s3_rw.secret}"
}
