#
# AWS settings
#

# Authentication for the AWS provider - we use this access
# key in order to be able to create a new AWS user and the S3
# bucket that the application needs.
variable "aws_access_key_id" {}

variable "aws_access_secret_key" {}

variable "aws_region" {
  default     = "eu-west-1"
  description = "AWS Region. Defaults to 'EU (Ireland)'."
}

# AWS and S3 settings - these define the details of the S3 bucket that we will create
# The credentials to access the bucket are created automatically by Terraform

variable "s3_bucket_name" {
  description = "Name to assign to the S3 Bucket."
}

variable "s3_resource_tag_name" {
  description = "Bucket 'name' tag."
}

variable "s3_resource_tag_environment" {
  description = "Bucket 'environment' tag."
}

variable "cloudfront_cnames" {
  description = "CloudFront CNAMEs"
  type        = "list"
}
